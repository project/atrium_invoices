<?php

/**
 * Implementation of hook_menu_default_items().
 */
function atrium_invoices_menu_default_items() {
  module_load_include('inc', 'atrium_invoices', 'atrium_invoices.defaults');
  $args = func_get_args();
  return call_user_func_array('_atrium_invoices_menu_default_items', $args);
}

/**
 * Implementation of hook_user_default_permissions().
 */
function atrium_invoices_user_default_permissions() {
  module_load_include('inc', 'atrium_invoices', 'atrium_invoices.defaults');
  $args = func_get_args();
  return call_user_func_array('_atrium_invoices_user_default_permissions', $args);
}

/**
 * Implementation of hook_views_default_views().
 */
function atrium_invoices_views_default_views() {
  module_load_include('inc', 'atrium_invoices', 'atrium_invoices.features.views');
  $args = func_get_args();
  return call_user_func_array('_atrium_invoices_views_default_views', $args);
}
