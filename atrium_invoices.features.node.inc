<?php

/**
 * Helper to implementation of hook_node_info().
 */
function _atrium_invoices_node_info() {
  $items = array(
    'invoice' => array(
      'name' => t('Invoice'),
      'module' => 'features',
      'description' => t('An invoice'),
      'has_title' => '0',
      'title_label' => '',
      'has_body' => '0',
      'body_label' => '',
    ),
  );
  return $items;
}
