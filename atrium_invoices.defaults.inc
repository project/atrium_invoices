<?php

/**
 * Helper to implementation of hook_menu_default_items().
 */
function _atrium_invoices_menu_default_items() {
  $items = array();

  $items[] = array(
    'title' => 'Invoices',
    'path' => 'invoices',
    'weight' => '10',
  );
  // Translatables
  array(
    t('Invoices'),
  );


  return $items;
}

/**
 * Helper to implementation of hook_user_default_permissions().
 */
function _atrium_invoices_user_default_permissions() {
  $permissions = array();

  // Exported permission: access invoices
  $permissions[] = array(
    'name' => 'access invoices',
    'roles' => array(
      '0' => 'authenticated user',
      '1' => 'admin',
      '2' => 'manager',
    ),
  );

  // Exported permission: administer invoices
  $permissions[] = array(
    'name' => 'administer invoices',
    'roles' => array(
      '0' => 'admin',
    ),
  );

  // Exported permission: administer own invoices
  $permissions[] = array(
    'name' => 'administer own invoices',
    'roles' => array(
      '0' => 'admin',
      '1' => 'manager',
    ),
  );

  return $permissions;
}
